const config = require('../../config/config');
const {google} = require('googleapis/build/src/index');
const axios = require('axios/index');

const gmailApi = {
  readonly: 'https://www.googleapis.com/auth/gmail.readonly',
  refresh_token: 'https://oauth2.googleapis.com/token',
};

const SCOPES = [gmailApi.readonly];

function createAuth() {
  const {client_id, secret, redirect_uri} = config;

  const oAuth2Client = new google.auth.OAuth2(client_id, secret, redirect_uri);
  return oAuth2Client;
}

function getAuthUrl(state) {
  const oAuth2Client = createAuth();
  const authUrl = oAuth2Client.generateAuthUrl({
    access_type: 'offline',
    scope: SCOPES,
    state,
  });
  return authUrl;
}

function refreshToken(refresh_token) {
  return axios
      .post(gmailApi.refresh_token, {
        refresh_token: refresh_token,
        client_id: config.client_id,
        client_secret: config.secret,
        grant_type: 'refresh_token',
      })
      .then(res => {
        return res.data.access_token;
      })
      .catch(e => {
        throw e;
      })
}

module.exports = {createAuth, getAuthUrl, refreshToken};
