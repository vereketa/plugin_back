const User = require('../models/User');
const Gmail = require('node-gmail-api');
const moment = require('moment/moment');
const telegramBot = require('../bot/telegramBot');
const { defaultButtons } = require('../bot/util');
const { refreshToken } = require('./authService');
const winston = require('../../config/winston');

class WatchGmail {
  constructor() {
    this._minute = 0.1;
    this._time = this._minute * 1000 * 60;
    this._emails = [];
  }

  async start() {
    this._emails = (await User.find({}, 'email').exec()).map(m => m.email);

    winston.log({
      level: 'info',
      message: `All emails start: ${this._emails}`
    });
    this._emails.forEach(email => {
      this.listingUser(email);
    });
  }

  addUser(email) {
    winston.log({
      level: 'info',
      message: `Add new user listing: ${email}`
    });
    this._emails.push(email);
    this.listingUser(email);
  }

  async listingUser(email) {
    setTimeout(async function __listing() {
      let stop = false;
      let first = false;
      let firstMessage = '';
      let isSendMessage = false;

      const user = await User.findOne({ email })
        .populate('keyWords')
        .exec();

      if (!user) {
        console.log('user not found!!');
        this._emails = this._emails.filter(e => e !== email);
        return;
      }

      const gmail = new Gmail(user.access_token);
      const s = gmail.messages('', {
        max: 10,
        fields: ['id', 'snippet', 'internalDate'],
      });

      s.on('error', async () => {
        console.log('error, ', user.refresh_token);
        const access_token = await refreshToken(user.refresh_token);
        winston.log({
          level: 'info',
          message: `Old token: ${user.access_token}; New token: ${access_token}`
        });
        user.access_token = access_token;
        await user.save();
        setTimeout(__listing, 1000);
      });
      s.on('data', d => {
        if (!first) {
          firstMessage = d.id;
          first = true;
        }
        if (user.idMessage == d.id) {
          stop = true;
        }

        if (stop) {
          return;
        }

        for (let word of user.keyWords) {
          if (d.snippet && d.snippet.toLowerCase().indexOf(word.text.toLowerCase()) !== -1) {
            const date = `${moment(new Date(+d.internalDate)).format(
              'MMMM Do YYYY, h:mm:ss a',
            )}\n`;
            const ref = `https://mail.google.com/mail/u/0/#inbox/${d.id}`;
            const body = `\n${d.snippet}`;

            telegramBot.sendMessage(
              user.chatId,
              `${date}${ref}${body}`,
              defaultButtons(d.id),
            );
            isSendMessage = true;
            return;
          }
        }
      });
      s.on('end', () => {
        user.idMessage = firstMessage;
        user.save();

        if (isSendMessage) {
          let options = {
            reply_markup: JSON.stringify({
              inline_keyboard: [
                [
                  {
                    text: 'Google Calendar',
                    url: 'https://calendar.google.com/calendar/r',
                    callback_data: '1',
                  },
                ],
              ],
            }),
          };
          telegramBot.sendMessage(user.chatId, 'Choose any button:', options);
        }
        setTimeout(__listing, this._time);
      });
    }, this._time);
  }
}

module.exports = { WatchGmail };
