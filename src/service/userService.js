const KeyWord = require('../models/KeyWord');
const mongoose = require('mongoose');
const User = require('../models/User');

const addKeys = async (chatId, text) => {
  const user = await User.findOne({ chatId }).populate('keyWords').exec();
  if (!user) {
    return {
      success: false,
    };
  }
  const keyWords = user.keyWords || [];
  const words = text.match(/\w*/g).filter(w => w.length > 0);
  const result = [];

  words.forEach(async word => {
    const newId2 = new mongoose.Types.ObjectId();
    const w = { text: word, _id: newId2 };
    result.push(w);
    keyWords.push(newId2);
    KeyWord.create(w);
  });
  user.keyWords = keyWords;
  await user.save();
  return {
    success: true,
    result
  };
};

module.exports = { addKeys };
