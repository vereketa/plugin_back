const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = new Schema({
  email: String,
  chatId: String,
  username: String,
  idMessage: String,
  keyWords: {
    type: [{
      type: Schema.ObjectId,
      ref: 'KeyWord'
    }],
    default: [],
  },
  access_token: String,
  refresh_token: String,
  expiry_date: Number,
});

const User = mongoose.model('User', userSchema);
module.exports = User;
