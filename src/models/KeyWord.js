const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const wordSchema = new Schema({
  text: String,
  _id: Schema.ObjectId,
});

const KeyWord = mongoose.model('KeyWord', wordSchema);
module.exports = KeyWord;
