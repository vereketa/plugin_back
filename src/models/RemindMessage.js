const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const messageSchema = new Schema({
  text: String,
  id: String,
});

const RemindMessage = mongoose.model('RemindMessage', messageSchema);
module.exports = RemindMessage;
