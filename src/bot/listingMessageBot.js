const User = require('../models/User');
const RemindMessage = require('../models/RemindMessage');
const actions = require('../gmail/constants');
const {defaultButtons, getAuthButtons, getTime} = require('./util');
const {addKeys} = require('../service/userService');
const telegramBot = require('./telegramBot');
const winston = require('../../config/winston');

const listingBotMessage = () => {
  telegramBot.on('callback_query', function (msg) {
    const data = JSON.parse(msg.data);
    const id = data.id;

    if (data.type === actions.REMIND) {
      winston.log({
        level: 'info',
        message: `callback_query: ${actions.REMIND}`
      });
      let options = {
        reply_markup: JSON.stringify({
          inline_keyboard: [
            [
              {
                text: '10 seconds',
                callback_data: JSON.stringify({
                  type: actions.REMIND_TIME,
                  time: `${getTime(0.1)}`,
                  id,
                }),
              },
              {
                text: '30 minutes',
                callback_data: JSON.stringify({
                  type: actions.REMIND_TIME,
                  time: `${getTime(30, 'm')}`,
                  id,
                }),
              },
              {
                text: '1 hour',
                callback_data: JSON.stringify({
                  type: actions.REMIND_TIME,
                  time: `${getTime(1, 'h')}`,
                  id,
                }),
              },
              {
                text: '2 hour',
                callback_data: JSON.stringify({
                  type: actions.REMIND_TIME,
                  time: `${getTime(2, 'h')}`,
                  id,
                }),
              },
            ],
          ],
        }),
      };

      RemindMessage.create({text: msg.message.text, id}, () => {
        telegramBot.sendMessage(msg.message.chat.id, 'Choose any button:', options);
      });
    } else if (data.type === actions.REMIND_TIME) {
      winston.log({
        level: 'info',
        message: `callback_query: ${actions.REMIND_TIME}`
      });
      telegramBot.sendMessage(msg.message.chat.id, `Reminder was saving`);
      const time = +data.time;
      setTimeout(() => {
        RemindMessage.findOneAndDelete({id}, (err, message) => {
          telegramBot.sendMessage(
              msg.message.chat.id,
              `Reminder\n${message.text}`,
              defaultButtons(id),
          );
        });
      }, time);
    }
  });

  telegramBot.onText(/\/start/, (msg, match) => {
    const chatId = msg.chat.id;
    const username = msg.from.username || (`${msg.from.first_name} ${msg.from.last_name}`);

    winston.log({
      level: 'info',
      message: `ChatId: ${chatId}; username: ${username}`
    });


    User.findOne({chatId}, (err, dov) => {
      if (err) {
        telegramBot.sendMessage(
            chatId,
            `Tyr again. Enter this command: \start`,
        );
        return;
      }
      if (!dov) {
        const state = JSON.stringify({chatId, username});
        telegramBot.sendMessage(chatId, `Choose any buttons: `, getAuthButtons(state));
      } else {
        telegramBot.sendMessage(chatId, 'You already subscribe');
      }
    });

  });

  telegramBot.onText(/\/add (.+)/, async (msg, match) => {
    const chatId = msg.chat.id;
    const text = match[1];
    await addKeys(chatId, text);
    telegramBot.sendMessage(chatId, `New keys add`);
  });
};


module.exports = {listingBotMessage};
