const TelegramBot = require('node-telegram-bot-api');
const config = require('../../config/config');

const token = config.token_bot;
const telegramBot = new TelegramBot(token, { polling: true });

module.exports = telegramBot;
