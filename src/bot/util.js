const actions = require('../gmail/constants');
const { getAuthUrl } = require('../service/authService');

const getUrlGmail = id => `https://mail.google.com/mail/u/0/#inbox/${id}`;

const defaultButtons = id => {
  let options = {
    reply_markup: JSON.stringify({
      inline_keyboard: [
        [
          {text: 'Reply to email', url: getUrlGmail(id), callback_data: '1'},
          {
            text: 'Remind later', callback_data: JSON.stringify({
              type: actions.REMIND,
              id
            })
          },
        ],
      ]
    })
  };
  return options;
};

const getAuthButtons = chatId => {
  return {
    reply_markup: JSON.stringify({
      inline_keyboard: [
        [
          {
            text: 'Auth with gmail',
            url: getAuthUrl(chatId),
            callback_data: '1',
          },
        ],
      ],
    }),
  };
};

const getTime = (time, str = 'm') => {
  if (str.toLowerCase() === 'm') {
    return time * 1000 * 60;
  } else if (str.toLowerCase() === 'h') {
    return time * 1000 * 60 * 60;
  }
};

module.exports = { defaultButtons, getAuthButtons, getTime };
