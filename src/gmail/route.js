const express = require('express');
const router = express.Router();
const { getUserByEmail, addKeysById, gmailRedirect} = require('./controllers');


router.get('/user', getUserByEmail);
router.post('/keys', addKeysById);
router.get('/auth/redirect', gmailRedirect);


module.exports = router;
