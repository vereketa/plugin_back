const actions = {
  REMIND: 'remind',
  REMIND_TIME: 'time'
};

module.exports = actions;
