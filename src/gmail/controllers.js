const axios = require('axios');
const User = require('../models/User');
const { createAuth } = require('../service/authService');
const telegramBot = require('../bot/telegramBot');
const {addKeys} = require('../service/userService');
const {WatchGmail} = require('../service/watchGmailService');

const getUserByEmail = async (req, res) => {
  const email = req.query.email;

  const user = await User.findOne({email})
      .populate('keyWords')
      .exec();

  if (user) {
    res.json({
      email,
      keyWords: user.keyWords,
      username: user.username || '',
      subscribe: true,
    });
  } else {
    res.json({
      email,
      subscribe: false,
    });
  }
};

const addKeysById = async (req, res) => {
  const {search, email} = req.body;

  const result = await addKeys(email, search);

  if (result.success) {
    return res.status(200).json(result);
  } else {
    return res.status(500).send('Invalid server')
  }
};

const gmailRedirect = async (req, res) => {
  const code = req.query.code;
  const state = JSON.parse(req.query.state);
  if (!state || !state.chatId) {
    return res.status(500).send('Invalid server(url).');
  }

  const user = await User.findOne({chatId: state.chatId}).exec();
  if (user) {
    telegramBot.sendMessage(state.chatId, 'You already subscribe');
    return res.sendStatus(200);
  }
  const oAuth2Client = createAuth();

  oAuth2Client.getToken(code, (err, token) => {
    if (err) {
      res.send('Invalid server(token).');
      return console.error('Error retrieving access token', err);
    }

    axios
        .get('https://www.googleapis.com/gmail/v1/users/me/profile', {
          headers: {
            Authorization: `Bearer ${token.access_token}`,
          },
        })
        .then(response => {
          const user = new User({
            email: response.data.emailAddress,
            chatId: state.chatId,
            username: state.username,
            access_token: token.access_token,
            refresh_token: token.refresh_token,
            expiry_date: token.expiry_date,
          });

          user.save(err => {
            if (err) {
              telegramBot.sendMessage(state.chatId, `Database error.`);
              return res.status(500).send('Invalid server');
            }
            telegramBot.sendMessage(
                state.chatId,
                `User with email: ${user.email} subscribe`,
            );
            new WatchGmail().addUser(user.email);
            return res.sendStatus(200);
          });
        })
        .catch(err => {
          res.send('Invalid server (me).');
        });
  });
};

module.exports = {getUserByEmail, addKeysById, gmailRedirect};
