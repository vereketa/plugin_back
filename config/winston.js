const appRoot = require('app-root-path');
const winston = require('winston');
const mkdirp  = require('mkdirp');
const { format} = winston;
const { combine, timestamp, prettyPrint  } = format;

const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

const getFileLogName = () => {
  const date = new Date();
  date.setHours(date.getHours() + 3);
  const hour = date.getHours();
  const path = `${appRoot}/logs/${date.getFullYear()}/${months[date.getMonth()]}/${date.getDate()}`;
  const fullPath = `${path}/${hour}_00-${hour + 1}_00.log`;
  mkdirp.sync(path);
  return fullPath;
};

const options = {
  file: {
    level: 'info',
    filename: getFileLogName(),
    handleExceptions: true,
    json: true,
    maxsize: 5242880, // 5MB
    maxFiles: 4,
    colorize: false,
  },
  console: {
    level: 'debug',
    handleExceptions: true,
    json: false,
    colorize: true,
  },
};

const logger = new winston.createLogger({
  format: combine(
      timestamp(),
      prettyPrint()
  ),
  transports: [
    new winston.transports.File(options.file),
    new winston.transports.Console(options.console)
  ],
  exitOnError: false, // do not exit on handled exceptions
});

logger.stream = {
  write: function(message, encoding) {
    logger.info(message);
  },
};

module.exports = logger;
