require('dotenv').config();
const config = {
  port: process.env.PORT,
  db: process.env.DB,
  token_bot: process.env.TOKEN_BOT,
  client_id: process.env.CLIENT_ID,
  secret: process.env.SECRET,
  redirect_uri: process.env.REDIRECT_URI
};

module.exports = config;
