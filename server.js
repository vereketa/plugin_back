const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const gmailRouter = require('./src/gmail/route');
const { listingBotMessage } = require('./src/bot/listingMessageBot');
const config = require('./config/config');
const app = express();
const morgan = require('morgan');
const { WatchGmail } = require('./src/service/watchGmailService');
const winston = require('./config/winston');
const {clientErrorHandler, errorHandler, logErrors} = require('./src/errors/errorsController');

app.use(morgan('combined', { stream: winston.stream }));
app.use(cors());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.get('/', (req, res) => {
  res.send('It works!');
});
app.use('/gmail', gmailRouter);

app.use(logErrors);
app.use(clientErrorHandler);
app.use(errorHandler);

mongoose.connect(config.db, { useNewUrlParser: true });
const db = mongoose.connection;
db.on('error', () => {
  winston.log({
    level: 'error',
    message: 'Connection error'
  });
});
db.once('open', () => {
  winston.log({
    level: 'info',
    message: 'Connect to DB'
  });
  listingBotMessage();
  const watch = new WatchGmail();
  watch.start();
  app.listen(config.port);
});
